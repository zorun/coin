# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.core.exceptions import ValidationError


def validate_v4(address):
    if address.version != 4:
        raise ValidationError('{} is not an IPv4 address'.format(address))


def validate_v6(address):
    if address.version != 6:
        raise ValidationError('{} is not an IPv6 address'.format(address))
