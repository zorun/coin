# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf.urls import patterns, url
from coin.offers.views import ConfigurationRedirectView

urlpatterns = patterns(
    '',
    # Redirect to the appropriate configuration backend.
    url(r'^configuration/(?P<id>.+)$', ConfigurationRedirectView.as_view(), name="configuration-redirect"),
)
