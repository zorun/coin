from django.template import Template, Library

register = Library()

@register.inclusion_tag('isp_database/includes/isp_address_multiline.html')
def multiline_isp_addr(branding):
    return {'branding': branding}

@register.filter
def pretty_iban(s):
    #FR764 2559 0001 2410 2002 3285 19
    return ' '.join([s[i:i+4] for i in xrange(0, len(s), 4)])
