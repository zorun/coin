The COIN project
================

`Coin` is Illyse's Information System, designed to manage both members
and Internet accesses, such as through DSL, VPN, wireless…

It is written in Django, and features a generic configuration interface,
which allows to implement custom backends for different technologies.
Currently implemented is a LDAP-based backend for OpenVPN, and a very
simple DSL backend, without any authentication (useful for "white label"
DSL).

Coin currently only works with python2, because `python-ldap` is (as of
2013) not compatible with python3.

The project page (issue, wiki, etc) is here:

  https://code.ffdn.org/FFDN/coin/


Demo
====

A demo of COIN is publicly available at:

  https://coin-dev.illyse.org

Login: ffdn
Password: internet

This user account has access to the administration interface.


Extending Coin
==============

If you want to write your own backend, see `EXTENDING.md`.


Quickstart
==========

Virtualenv
----------

Using a virtualenv is recommended.  On Debian, install `python-virtualenv`.
On Archlinux, the package is called `python2-virtualenv`, and you must
replace the `virtualenv` command with `virtualenv2` in the following.

To create the virtualenv (the first time):

  virtualenv ~/tmp/venv-illyse


To activate the virtualenv (you need to do this each time you work on
the project):

  . ~/tmp/venv-illyse/bin/activate


Install dependencies. On Debian, you will probably need the
`python-dev`, `python-pip`, `libldap-dev`, `libpq-dev` and `libsasl2-dev`
packages.

  sudo apt-get install python-dev python-pip libldap2-dev libpq-dev libsasl2-dev

Then run:

  pip install -r requirements.txt

You may experience problems with SSL certificates du to self-signed cert used by code.ffdn.org. You can temporarily disable certificate verification in git : git config --global http.sslVerify false

You should now be able to run `python manage.py` (within the
virtualenv, obviously) without error.

Settings
--------

The `coin/settings_local.py` file is ignored by Git: feel free to override any
setting by writing into that file. For example, to override the `DEBUG`
settings:

    echo '# -*- coding: utf-8 -*-' > coin/settings_local.py
    echo 'DEBUG = TEMPLATE_DEBUG = True' >> coin/settings_local.py

If you don't want to use LDAP, just set in your `settings_local.py`:

    LDAP_ACTIVATE = False

See the end of this README for a reference of available configuration settings.

Database
--------

At this point, you should setup your database: we highly recommend PostgreSQL.
SQLite might work, but some features will not be available:

- automatic allocation of IP subnets (needs proper subnet implementation in
  the database)
- sending automated emails to remind of expiring membership fee
  (needs aggregation on date fields, see Django doc)

For more information on the database setup, see:

  https://www.illyse.org/projects/ils-si/wiki/Mise_en_place_environnement_de_dev

The first time, you need to create the database, create a superuser, and
import some base data to play with:

    python manage.py migrate
    python manage.py createsuperuser
    python manage.py loaddata offers ip_pool

Note that the superuser will be inserted into the LDAP backend exactly in the
same way as all other members, so you should use a real account (not just
admin/admin).

Then, at each code update, you will only need to update dependencies and apply
new migrations:

    pip install -r requirements.txt
    python manage.py migrate


At this point, Django should run correctly:

    python manage.py runserver


Available commands
==================

Some useful administration commands are available via `manage.py`.

`python manage.py members_email`: returns email addresses of all members, one
per line.  This may be useful to automatically feed a mailing list software.
Note that membership is based on the `status` field of users, not on
membership fees.  That is, even if a member has forgot to renew his or her
membership fee, his or her address will still show up in this list.

`python manage.py charge_subscriptions`: generate invoices (including a
PDF version) for each subscriber.  You probably want to run this command
every month as a cron task, see below.

`python manage.py call_for_membership_fees`: send reminder emails to members
whose membership fee is about to expire or is already expired (1 month before,
on the day of expiration, 1 month after, 2 months after, and 3 months after).
You should run this command in a cron job every day.


Configuration
=============

You should first setup the `sites` application, in the admin.  The domain name
configured there is used for outgoing emails.

LDAP
----

By default, LDAP support is disabled.  If you want to use LDAP (for
instance for the OpenVPN/LDAP backend, or for your own backend), see the
configuration file `coin/settings_local.example-illyse.py`.

ISP-specific configuration
--------------------------

The main place to customize Coin for your ISP is the IspDatabase application,
in the admin.  Information entered in this application has two purposes:

1) generate a JSON file usable by http://db.ffdn.org/

2) customize the user interface (postal address, bank account, ISP name & logo...)


Some bits of configuration are done in `settings.py`: LDAP branches, RSS feeds
to display on the home page, and so on.

Cron tasks
----------

You may want to run cron jobs for repetitive tasks.

The command that generates invoices should be run every day, so that invoices
for new subscriptions are created automatically.  On the first day of
each month, invoices will be generated for all subscriptions.
This will run the command at 23:59 every day:

    59 23 * * * /home/coin/venv/bin/python manage.py charge_subscriptions`

To send reminder emails for membership fee expiration:

    42 3 * * * /home/coin/venv/bin/python manage.py call_for_membership_fees`

If you want to be notified by email each time a reminder email is sent, you
can set an email address in the crontab:

    MAILTO=tresorier@myisp.fr

More information
================

For the rest of the setup (database, LDAP), see

  https://www.illyse.org/projects/ils-si/wiki/Mise_en_place_environnement_de_dev

For real production deployment, see file `DEPLOYMENT.md`.
