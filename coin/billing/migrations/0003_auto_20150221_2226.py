# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('billing', '0002_auto_20141002_0204'),
    ]

    operations = [
        migrations.AlterField(
            model_name='invoice',
            name='validated',
            field=models.BooleanField(default=False, help_text='Once validated, a PDF is generated and the invoice cannot be modified', verbose_name='valid\xe9e'),
            preserve_default=True,
        ),
    ]
