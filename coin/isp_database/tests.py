from django.test import TestCase

# Create your tests here.

from coin.isp_database.templatetags.isptags import *

class TestPrettifiers(TestCase):
    def test_pretty_iban(self):
        """ Prints pretty readable IBAN

        Takes the IBAN in compact form and displays it according to the display spec
        See http://en.wikipedia.org/wiki/International_Bank_Account_Number#Practicalities
        """
        self.assertEqual(pretty_iban('DEkkBBBBBBBBCCCCCCCCCC'),
                         'DEkk BBBB BBBB CCCC CCCC CC')
        self.assertEqual(pretty_iban('ADkkBBBBSSSSCCCCCCCCCCCC'),
                         'ADkk BBBB SSSS CCCC CCCC CCCC')
        self.assertEqual(pretty_iban(''), '')
