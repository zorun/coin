# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import netfields.fields
import coin.resources.models
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('configuration', '0002_auto_20141002_0204'),
    ]

    operations = [
        migrations.CreateModel(
            name='IPPool',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(help_text="Nom du pool d'IP", max_length=255, verbose_name='nom')),
                ('default_subnetsize', models.PositiveSmallIntegerField(help_text='Taille par d\xe9faut du sous-r\xe9seau \xe0 allouer aux abonn\xe9s dans ce pool', verbose_name='taille de sous-r\xe9seau par d\xe9faut', validators=[django.core.validators.MaxValueValidator(64)])),
                ('inet', netfields.fields.CidrAddressField(help_text="Bloc d'adresses IP du pool", max_length=43, verbose_name='r\xe9seau', validators=[coin.resources.models.validate_subnet])),
            ],
            options={
                'verbose_name': "pool d'IP",
                'verbose_name_plural': "pools d'IP",
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='IPSubnet',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('inet', netfields.fields.CidrAddressField(blank=True, help_text='Laisser vide pour allouer automatiquement', max_length=43, verbose_name='sous-r\xe9seau', validators=[coin.resources.models.validate_subnet])),
                ('delegate_reverse_dns', models.BooleanField(default=False, help_text='D\xe9l\xe9guer la r\xe9solution DNS inverse de ce sous-r\xe9seau \xe0 un ou plusieurs serveurs de noms', verbose_name='d\xe9l\xe9guer le reverse DNS')),
                ('configuration', models.ForeignKey(related_name='ip_subnet', verbose_name='configuration', to='configuration.Configuration')),
                ('ip_pool', models.ForeignKey(verbose_name="pool d'IP", to='resources.IPPool')),
            ],
            options={
                'verbose_name': 'sous-r\xe9seau IP',
                'verbose_name_plural': 'sous-r\xe9seaux IP',
            },
            bases=(models.Model,),
        ),
    ]
