# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.views.generic.base import RedirectView
from django.shortcuts import get_object_or_404
from django.core.urlresolvers import reverse

from coin.offers.models import OfferSubscription


class ConfigurationRedirectView(RedirectView):
    """Redirects to the appropriate view for the configuration backend of the
    specified subscription."""

    permanent = False

    def get_redirect_url(self, *args, **kwargs):
        subscription = get_object_or_404(OfferSubscription, pk=self.kwargs['id'],
                                         member=self.request.user)
        return reverse(subscription.configuration.url_namespace + ':' + subscription.configuration.backend_name,
                       args=[subscription.configuration.pk])
