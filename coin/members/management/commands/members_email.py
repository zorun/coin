# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.core.management.base import BaseCommand, CommandError

from coin.members.models import Member


class Command(BaseCommand):
    help = 'Returns the email addresses of all members, in a format suitable for bulk importing in Sympa'

    def handle(self, *args, **options):
        emails = [m.email for m in Member.objects.filter(status='member')]
        for email in emails:
            self.stdout.write(email)
