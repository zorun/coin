# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import netfields.fields
import coin.resources.models


class Migration(migrations.Migration):

    dependencies = [
        ('resources', '0002_ipsubnet_name_server'),
    ]

    operations = [
        migrations.AlterField(
            model_name='ipsubnet',
            name='inet',
            field=netfields.fields.CidrAddressField(validators=[coin.resources.models.validate_subnet], max_length=43, blank=True, help_text='Laisser vide pour allouer automatiquement', unique=True, verbose_name='sous-r\xe9seau'),
            preserve_default=True,
        ),
    ]
