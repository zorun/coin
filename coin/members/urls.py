# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf.urls import patterns, url
from django.views.generic import DetailView
from coin.members import views
from coin.members.models import Member


urlpatterns = patterns(
    '',
    url(r'^$', views.index, name='index'),
    url(r'^login/$', 'django.contrib.auth.views.login',
        {'template_name': 'members/registration/login.html'},
        name='login'),
    url(r'^logout/$', 'django.contrib.auth.views.logout_then_login',
        name='logout'),

    url(r'^password_change/$', 'django.contrib.auth.views.password_change',
        {'post_change_redirect': 'members:password_change_done',
         'template_name': 'members/registration/password_change_form.html'},
        name='password_change'),
    url(r'^password_change_done/$', 'django.contrib.auth.views.password_change_done',
        {'template_name': 'members/registration/password_change_done.html'},
        name='password_change_done'),

    url(r'^password_reset/$', 'django.contrib.auth.views.password_reset',
        {'post_reset_redirect': 'members:password_reset_done',
         'template_name': 'members/registration/password_reset_form.html',
         'email_template_name': 'members/registration/password_reset_email.html',
         'subject_template_name': 'members/registration/password_reset_subject.txt'},
        name='password_reset'),
    url(r'^password_reset/done/$', 'django.contrib.auth.views.password_reset_done',
        {'template_name': 'members/registration/password_reset_done.html',
         'current_app': 'members'},
        name='password_reset_done'),
    url(r'^password_reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$', 'django.contrib.auth.views.password_reset_confirm',
        {'post_reset_redirect': 'members:password_reset_complete',
         'template_name': 'members/registration/password_reset_confirm.html'},
        name='password_reset_confirm'),
    url(r'^password_reset/complete/$', 'django.contrib.auth.views.password_reset_complete',
        {'template_name': 'members/registration/password_reset_complete.html'},
        name='password_reset_complete'),


    url(r'^detail/$', views.detail,
        name='detail'),

    url(r'^subscriptions/', views.subscriptions, name='subscriptions'),
    # url(r'^subscription/(?P<id>\d+)', views.subscriptions, name = 'subscription'),

    url(r'^invoices/', views.invoices, name='invoices'),
    url(r'^contact/', views.contact, name='contact'),
)
