# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

from coin.configuration.models import Configuration


class SimpleDSL(Configuration):
    """Very simple DSL model, mostly to demonstrate the use of the generic
    functionality of COIN.  There is no real configuration backend, and no
    authentication data.  But this still allows to track the phone number
    and IP addresses of subscribers, which may be useful for "white label"
    DSL reselling.
    """
    class Meta:
        verbose_name = 'DSL line'
        # If Django's default pluralisation is not satisfactory
        #verbose_name_plural = 'very many DSL lines'

    # URL namespace associated to this configuration type, to build URLs
    # in various view.  Should also be defined in urls.py.  Here, we don't
    # define any view, so there's no need for an URL namespace.
    #url_namespace = "dsl"
    phone_number = models.CharField(max_length=20,
                                    verbose_name='phone number',
                                    help_text="Phone number associated to the DSL line")
    
    def __unicode__(self):
        return self.phone_number

    def subnet_event(self):
        # Do something with self.ip_subnet.all() here.
        pass
