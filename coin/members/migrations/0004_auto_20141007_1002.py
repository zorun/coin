# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('members', '0003_auto_20141007_0956'),
    ]

    operations = [
        migrations.AlterField(
            model_name='membershipfee',
            name='amount',
            field=models.DecimalField(default=settings.MEMBER_DEFAULT_COTISATION, help_text='en \u20ac', verbose_name='montant', max_digits=5, decimal_places=2),
        ),
    ]
