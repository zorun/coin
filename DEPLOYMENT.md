For production deployment, it is recommended to use a reverse proxy
setup, for instance using gunicorn.

This is because the access to invoices (PDF) is a bit special: they
are served by the web server for efficiency, but django itself handles
authorisation.  This needs special support from the web server
(mod_xsendfile for Apache, etc).

The following assumes Debian wheezy, with either Apache or Nginx as
frontend.

For the complete deployment configuration used by Illyse, see:

  https://www.illyse.org/projects/ils-si/wiki/Mise_en_place_production


## Gunicorn configuration

Gunicorn on Debian wheezy is very nice, because it can launch automatically
at boot.  Of course, you can also use supervisord.

Install the package:

    $ sudo apt-get install gunicorn


Gunicorn is supposed to handle Django out-of-the-box, but unfortunately,
it does not work anymore with Django 1.7.  Anyway, this method is deprecated
upstream.  We will simply use WSGI.  See also

  https://docs.djangoproject.com/en/1.7/howto/deployment/wsgi/gunicorn/


The Gunicorn config is the following:

    # cat /etc/gunicorn.d/coin 
    CONFIG = {
        'working_dir': '/home/myuser/coin',
        'python': '/home/myuser/virtualenv/bin/python',
        'user': 'www-data',
        'group': 'www-data',
        'args': (
            '--bind=127.0.0.1:8484',
            '--workers=5',
            #'--preload',
            'coin.wsgi',
        ),
    }

Launching it is as simple as

    # service gunicorn start

Hint: to debug gunicorn if it does not work, uncomment `--preload`
and look at `/var/log/gunicorn/coin.log`.  Otherwise, you won't
get any traceback.


## Nginx configuration

    server {
            listen [::]:80;
            server_name coin.plop;
            rewrite ^(.*) https://$server_name$1 permanent;
    }
    
    server {
            listen [::]:443 ssl;
            server_name coin.plop;

            ssl on;
            ssl_certificate /etc/ssl/coin-plop.crt;
            ssl_certificate_key /etc/ssl/coin-plop.pem;
            root /var/www/;
            access_log /var/log/nginx/coin.plop_ssl_access_log main;
            error_log /var/log/nginx/coin.plop_ssl_error_log info;
    
            proxy_redirect off;
            proxy_set_header Host $http_host;
            proxy_set_header X-Real-IP $remote_addr;
  
            location /static/ {
                    alias /home/coin/coin/coin/static/;
            }
            # Invoices, see SENDFILE_* options in coin
            location /protected/ {
                    internal;
                    alias /home/coin/coin/smedia/;
            }
            location / {
                    proxy_pass http://localhost:8484;
            }
    }


## Apache configuration

    <VirtualHost *:80>
            ServerName      coin.plop
            Redirect permanent / https://coin.plop/
    </VirtualHost>
    
    <VirtualHost *:443>
        ServerName coin.plop
    
        DocumentRoot /home/myuser/coin/coin
    
        <Proxy *>
            Order deny,allow
            Allow from all
        </Proxy>
    
        ProxyPass /robots.txt !
        ProxyPass /favicon.ico !
        ProxyPass /static/ !
    
        ProxyPass / http://localhost:8484/
        ProxyPreserveHost On
    
        Alias /robots.txt /home/myuser/coin/coin/static/robots.txt
        Alias /favicon.ico /home/myuser/coin/coin/static/img/favicon.ico
    
        <Directory /home/illysedev/coin/coin/static>
            Order deny,allow
            Allow from all
            Options -Indexes
        </Directory>
    
        ErrorLog ${APACHE_LOG_DIR}/error.log
    
        # Possible values include: debug, info, notice, warn, error, crit,
        # alert, emerg.
        LogLevel warn
    
        CustomLog ${APACHE_LOG_DIR}/access.log combined
    
    	SSLEngine On
            SSLCertificateFile    /etc/ssl/certs/illyse-coin-cert.pem
            SSLCertificateKeyFile /etc/ssl/private/illyse-coin-privkey.pem

	# Directly send invoices, avoid Django to do it
	XSendFile On
	XSendFilePath /home/myuser/coin/smedia/
    
    </VirtualHost>
