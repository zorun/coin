# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import coin.billing.models
import coin.utils
import datetime
import django.core.files.storage


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Invoice',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('validated', models.BooleanField(default=False, verbose_name='valid\xe9e')),
                ('number', models.CharField(default=coin.billing.models.next_invoice_number, unique=True, max_length=25, verbose_name='num\xe9ro')),
                ('status', models.CharField(default='open', max_length=50, verbose_name='statut', choices=[('open', 'A payer'), ('closed', 'Regl\xe9e'), ('trouble', 'Litige')])),
                ('date', models.DateField(default=datetime.date.today, null=True, verbose_name='date')),
                ('date_due', models.DateField(default=coin.utils.end_of_month, null=True, verbose_name="date d'\xe9ch\xe9ance de paiement")),
                ('pdf', models.FileField(storage=coin.utils.private_files_storage, upload_to=coin.billing.models.invoice_pdf_filename, null=True, verbose_name='PDF', blank=True)),
            ],
            options={
                'verbose_name': 'facture',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='InvoiceDetail',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('label', models.CharField(max_length=100)),
                ('amount', models.DecimalField(verbose_name='montant', max_digits=5, decimal_places=2)),
                ('quantity', models.DecimalField(default=1.0, null=True, verbose_name='quantit\xe9', max_digits=4, decimal_places=2)),
                ('tax', models.DecimalField(decimal_places=2, default=0.0, max_digits=4, help_text='en %', null=True, verbose_name='TVA')),
                ('period_from', models.DateField(default=coin.utils.start_of_month, help_text='Date de d\xe9but de p\xe9riode sur laquelle est factur\xe9 cet item', null=True, verbose_name='d\xe9but de p\xe9riode', blank=True)),
                ('period_to', models.DateField(default=coin.utils.end_of_month, help_text='Date de fin de p\xe9riode sur laquelle est factur\xe9 cet item', null=True, verbose_name='fin de p\xe9riode', blank=True)),
                ('invoice', models.ForeignKey(related_name='details', verbose_name='facture', to='billing.Invoice')),
            ],
            options={
                'verbose_name': 'd\xe9tail de facture',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Payment',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('payment_mean', models.CharField(default='transfer', max_length=100, null=True, verbose_name='moyen de paiement', choices=[('cash', 'Esp\xe8ces'), ('check', 'Ch\xe8que'), ('transfer', 'Virement'), ('other', 'Autre')])),
                ('amount', models.DecimalField(null=True, verbose_name='montant', max_digits=5, decimal_places=2)),
                ('date', models.DateField(default=datetime.date.today)),
                ('invoice', models.ForeignKey(related_name='payments', verbose_name='facture', to='billing.Invoice')),
            ],
            options={
                'verbose_name': 'paiement',
            },
            bases=(models.Model,),
        ),
    ]
