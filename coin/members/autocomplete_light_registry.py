# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import autocomplete_light
from models import Member

# This will generate a MemberAutocomplete class
autocomplete_light.register(Member,
                            # Just like in ModelAdmin.search_fields
                            search_fields=[
                                '^first_name', 'last_name', 'organization_name',
                                'username'],
                            # This will actually data-minimum-characters which
                            # will set widget.autocomplete.minimumCharacters.
                            autocomplete_js_attributes={
                                'placeholder': 'Other model name ?', },
                            )
